/**
 * This imports the Earth class specifically from the planet package.
 * Without importing, the main class would only be able to access classes in 
 * the same package as itself.
 * 
 * IMPORTANT: importing packages is not a replacement for polymorphism,
 * as packages only contain various classes, not subtypes and supertypes.
 * Think of it like a folder hierarchy, not different instances of objects.
 */
import planet.Earth;

/**
 * This imports everything inside the land package, which is located
 * inside of the planet package.
 */
import planet.land.*;

/**
 * This imports everything in the water package, which is again
 * located inside of the planet package.
 */
import planet.water.*;

/**
 * @author Zach Evans
 * This main class is located in the default package, so no package is declared.
 * 
 * Interesting tip: package names are traditionally named beginning with a lowercase letter,
 * while Class names begin with a capital letter.
 */
public class Main {
	/**
	 * This is the main method, which prints out the various things on our example planet.
	 */
	public static void main(String[] args) {
		System.out.printf("This is the Main class, which is usually placed in the default package.\n\nThe other package we find is the planet package, which contains the Earth class:\n\n");
		
		System.out.println(Earth.description);
		
		System.out.printf("\nAlso inside the planet packages are land and water packages.\nLet's access the Continent class inside of land:\n\n");
		
		System.out.println(Continent.names);
		
		System.out.printf("\nAlso included in the planet package is a second package called water.\nInside we find the Ocean class, printed below:\n\n");
		
		System.out.println(Ocean.names);
	}
}
