/**
 * This is the package declaration statement. This tells us 
 * that we find this class within the planet package.
 */
package planet;

/**
 * This simple class contains a string describing planet earth.
 * @author Zach Evans
 *
 */
public class Earth {
	/**
	 * For simplicity's sake, this string is public.
	 */
	public static String description="This is planet Earth. It is found in the planet package.";
}
