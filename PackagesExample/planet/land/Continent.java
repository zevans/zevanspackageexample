/**
 * This is the package declaration statement. This tells us 
 * that we find this class within the land package,
 * which is found in the planet package.
 */
package planet.land;

/**
 * This is another simple class containing a data member which lists
 * the seven continents.
 * @author Zach Evans
 *
 */
public class Continent {
	public static String names="North America, South America, Europe, Africa, Asia, Australia, and Antarctica.";
}
