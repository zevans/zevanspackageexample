/**
 * This is the package declaration statement. This tells us 
 * that we find this class within the water package, which
 * is found inside the planet package.
 */
package planet.water;

/**
 * This is yet another simple class, which contains a
 * data member listing the five oceans.
 * @author Zach Evans
 *
 */
public class Ocean {
	public static String names="Atlantic, Pacific, Indian, Arctic, and Southern(Antarctic).";
}
